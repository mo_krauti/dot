import subprocess, argparse
#generic funcs
def check_root():
    id = int(subprocess.check_output("id").decode().split("(")[0].split("=")[1])
    if id != 0:
        print("You are not root!")
        exit()

#module funcs
def user(arg):
    if arg[0] == "list":
        print("root")
        f = open("/etc/passwd", "r")
        for i in f:
            s = i.split(":")
            if int(s[2]) >= 1000 and int(s[2]) != 65534:
                print(i.split(":")[0])
    if arg[0] == "password":
        if len(arg) == 2:
            subprocess.call("passwd")
        else:
            subprocess.call(["passwd", arg[2]])
    if arg[0] == "shell":
        if len(arg) == 3:
            subprocess.check_output(["chsh", "-s", str(arg[2])])
        else:
            subprocess.check_output(["chsh", "-s", str(arg[2]), str(arg[3])])

def time(arg):
    if arg[0] == "sync":
        pass


#module detection
check_root()
parser = argparse.ArgumentParser(description='LINUX Settings')
parser.add_argument('module', help="The Module you want to operate on")
parser.add_argument('function', help="The Function you want to use")
args = parser.parse_args()
exec(args.module+'("'+args.function+'")')
