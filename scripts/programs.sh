selection=$(echo "ARandR,VSCode,Steam,VLC,Wireshark" | rofi -sep "," -dmenu -p "Program Launcher")
case $selection in
	"VSCode") cd /home/mo/.code/bin && ./vscodium
	;;
	"VLC") exec vlc
	;;
	"Wireshark") wireshark-gtk
	;;
	"ARandR") arandr
	;;
	"Steam") steam
	;;
esac
