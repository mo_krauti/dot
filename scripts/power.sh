selection=$(echo "lock,shutdown,soft-reboot,reboot" | rofi -sep "," -dmenu -p "Power Management")
case $selection in
	"lock") exec /home/mo/scripts/lock.sh
	;;
	"shutdown") poweroff
	;;
	"soft-reboot") init 0 && init 5
	;;
	"reboot") reboot
	;;
esac

